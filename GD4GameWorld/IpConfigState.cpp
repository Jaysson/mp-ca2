#include "IpConfigState.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"


#include <SFML/Graphics/RenderWindow.hpp>

IpConfigState::IpConfigState(StateStack & stack, Context context) : State(stack, context)
, mGUIContainer()
, mWindow(*context.window)
{
	mBackgroundSprite.setTexture(context.textures->get(TextureIDs::TitleScreen));
	mIpAddress.setFont(context.fonts->get(FontIDs::Main));
	mIpAddress.setString(mIpAd);
	mIpAddress.setCharacterSize(35);
	mIpAddress.setFillColor(sf::Color::White);
	centreOrigin(mIpAddress);
	mIpAddress.setPosition(mWindow.getSize().x / 2.f, mWindow.getSize().y / 2.f);
}

void IpConfigState::draw()
{
	sf::RenderWindow& window = *getContext().window;

	window.draw(mBackgroundSprite);
	window.draw(mIpAddress);
}

bool IpConfigState::update(sf::Time dt)
{
	return true;
}

bool IpConfigState::handleEvent(const sf::Event & event)
{
	if (event.type == sf::Event::TextEntered)
	{
		mIpAd += event.text.unicode;
		mIpAddress.setString(mIpAd);
	}

	if (event.key.code == sf::Keyboard::Return)
	{
		if (!validateIpAddress(mIpAd))
		{
			mIpAd = "127.0.0.1";
		}
		requestStackPush(StateIDs::Menu);
	}
	return false;
}

bool IpConfigState::validateIpAddress(const std::string &ipAddress)
{
	//if (ipAddress.length()) {
	//	vector<std::string> _ip = split(ipAddress, '.');
	//	if (_ip.size() == 4) {
	//		for (int i = 0; i < 4; i++) {
	//			for (int j = 0; j < _ip[i].length(); j++)
	//				if (!isdigit(_ip[i][j])) return false;
	//			if ((atoi(_ip[i].c_str()) < 0) || (atoi(_ip[i].c_str()) > 255)) return false;
	//		}
	//		return true;
	//	}
	//}
	//return false;
	return false;
}