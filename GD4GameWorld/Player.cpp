#include "Player.hpp"
#include "CommandQueue.hpp"
#include "Character.hpp"
#include "Foreach.hpp"
#include "NetworkProtocol.hpp"

#include <SFML/Network/Packet.hpp>

#include <map>
#include <string>
#include <algorithm>
#include <iostream>

using namespace std::placeholders;

struct CharacterMover
{
	CharacterMover(float vx, float vy, int identifier) : velocity(vx, vy), playerID(identifier)
	{

	}
	void operator() (Character& character, sf::Time) const
	{
		if (character.getIdentifier() == playerID)
		{
			character.accelerate(velocity * character.getMaxSpeed());
			if ((velocity.x > 0) && !character.isFacingRight())
			{
				character.setFacingRight(true);
				//std::cout << velocity.x << std::endl;
			}
			else if ((velocity.x < 0) && character.isFacingRight())
			{
				character.setFacingRight(false);
				//std::cout << velocity.x  << std::endl;
			}
		}
	}
	sf::Vector2f velocity;
	int playerID;
};

struct CharacterJumpTrigger
{
	CharacterJumpTrigger(int identifier)
		: playerID(identifier)
	{
	}

	void operator() (Character& character, sf::Time dt) const
	{
		if (character.getIdentifier() == playerID)
		{
			character.beginJump();
		}
	}

	int playerID;
};

Player::Player(sf::TcpSocket* socket, sf::Int32 identifier, const KeyBinding* binding)
	: mCurrentMissionStatus(MissionStatus::MissionRunning)
	, hasLost(true)
	, mKeyBinding(binding)
	, mIdentifier(identifier)
	, mSocket(socket)
	, mMarkedForRemoval(false)
{
	mTimer.Start(true);
	initializeActions();

	//set initial action bindings
	//Assign all categories to the player's Character
	for (auto& pair : mActionBinding)
	{
		pair.second.category = static_cast<unsigned int>(Category::PlayerCharacter);
	}
}

bool Player::isLocal() const
{
	return mKeyBinding != nullptr;
}

void Player::handleEvent(const sf::Event& event, CommandQueue& commands)
{
	// Event
	if (event.type == sf::Event::KeyPressed)
	{
		Action action;
		if (mKeyBinding && mKeyBinding->checkAction(event.key.code, action) && !isRealtimeAction(action))
		{
			// Network connected -> send event over network
			if (mSocket)
			{
				sf::Packet packet;
				packet << static_cast<sf::Int32>(Client::PlayerEvent);
				packet << mIdentifier;
				packet << static_cast<sf::Int32>(action);
				mSocket->send(packet);
			}

			// Network disconnected -> local event
			else
			{
				commands.push(mActionBinding[action]);
			}
		}
	}

	// Realtime change (network connected)
	if ((event.type == sf::Event::KeyPressed || event.type == sf::Event::KeyReleased) && mSocket)
	{
		Action action;
		if (mKeyBinding && mKeyBinding->checkAction(event.key.code, action) && isRealtimeAction(action))
		{
			// Send realtime change over network
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Client::PlayerRealtimeChange);
			packet << mIdentifier;
			packet << static_cast<sf::Int32>(action);
			packet << (event.type == sf::Event::KeyPressed);
			mSocket->send(packet);
		}
	}
}

void Player::disableAllRealtimeActions()
{
	FOREACH(auto& action, mActionProxies)
	{
		sf::Packet packet;
		packet << static_cast<sf::Int32>(Client::PlayerRealtimeChange);
		packet << mIdentifier;
		packet << static_cast<sf::Int32>(action.first);
		packet << false;
		mSocket->send(packet);
	}
}
void Player::handleRealtimeInput(CommandQueue& commands)
{
	// Check if this is a networked game and local player or just a single player game
	if ((mSocket && isLocal()) || !mSocket)
	{
		// Lookup all actions and push corresponding commands to queue
		std::vector<Action> activeActions = mKeyBinding->getRealtimeActions();
		FOREACH(Action action, activeActions)
			commands.push(mActionBinding[action]);
	}
}
void Player::handleRealtimeNetworkInput(CommandQueue& commands)
{
	if (mSocket && !isLocal())
	{
		// Traverse all realtime input proxies. Because this is a networked game, the input isn't handled directly
		FOREACH(auto pair, mActionProxies)
		{
			if (pair.second && isRealtimeAction(pair.first))
				commands.push(mActionBinding[pair.first]);
		}
	}

}
void Player::handleNetworkEvent(Action action, CommandQueue& commands)
{
	commands.push(mActionBinding[action]);
}

void Player::handleNetworkRealtimeChange(Action action, bool actionEnabled)
{
	mActionProxies[action] = actionEnabled;
}

bool Player::isMarkedForRemoval()
{
	return mMarkedForRemoval;
}

void Player::setMarkedForRemoval()
{
	mMarkedForRemoval = true;
}

sf::Int32 Player::getIdentifier()
{
	return mIdentifier;
}
std::string Player::getTime()
{
	return mTimer.getTimeMins();
}

void Player::pauseTimer()
{
	mTimer.Stop();
}

void Player::unpauseTimer()
{
	mTimer.Start();
}

void Player::resetTimer()
{
	mTimer.Start(true);
}
void Player::setMissionStatus(MissionStatus status)
{
	mCurrentMissionStatus = status;
}

Player::MissionStatus Player::getMissionStatus() const
{
	return mCurrentMissionStatus;
}

void Player::initializeActions()
{
	//KG Checking to see if pad is connected

	//Player 1 with identifier 0
	mActionBinding[Action::MoveLeft].action = derivedAction<Character>(CharacterMover(-2, 0.f, mIdentifier));
	mActionBinding[Action::MoveRight].action = derivedAction<Character>(CharacterMover(2, 0.f, mIdentifier));
	mActionBinding[Action::Jump].action = derivedAction<Character>(CharacterJumpTrigger(mIdentifier));
}
