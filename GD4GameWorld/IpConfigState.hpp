#pragma once
#include "State.hpp"
#include "KeyBinding.hpp"""
#include "Container.hpp"
#include "Button.hpp"
#include "Label.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include <array>


class IpConfigState : public State
{
public:
	IpConfigState(StateStack& stack, Context context);

	virtual void					draw();
	virtual bool					update(sf::Time dt);
	virtual bool					handleEvent(const sf::Event& event);
	bool                            validateIpAddress(const std::string &ipAddress);


private:
	sf::Sprite					mBackgroundSprite;
	GUI::Container				mGUIContainer;
	sf::Text                    mIpAddress;
	sf::RenderWindow&			mWindow;
	std::string&                 mIpAd;
};